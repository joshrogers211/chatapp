﻿using ChatApp.Data;
using ChatApp.Data.Time;
using ChatApp.Models;
using ChatApp.Models.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatApp.ViewModels
{
    public class MessagesViewModel
    {
        private readonly Dictionary<Settings, object> _settings;
        private MessageGroup _currentMessageGroup;
        private Dictionary<long, Message> _messages;

        public MessagesViewModel(Dictionary<Settings, object> settings)
        {
            _settings = settings;
        }

        public List<MessageGroup> Messages { get; }

        public void AddMessage(Message message)
        {
            if ((_currentMessageGroup.Sender == message.Sender)
                && TimeHelper.ShouldMessagesGroup(_currentMessageGroup.FirstMessageTime, _settings[Settings.MessageGroupingTime])
                && _currentMessageGroup.AddMessage(message))
            {
                 return;
            }

            _currentMessageGroup = new MessageGroup(message);
            Messages.Add(_currentMessageGroup);
        }
    }
}
