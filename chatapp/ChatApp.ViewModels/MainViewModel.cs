using GalaSoft.MvvmLight;

namespace ChatApp.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()
        {
            if (IsInDesignMode)
            {
                Title = "Chat! (design)";
            }
            else
            {
                Title = "Chat!";
            }
        }

        public string Title { get; }
    }
}