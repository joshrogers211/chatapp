﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatApp.Models
{
    public class User : IEquatable<User>
    {
        public User (long id, string name)
        {
            Id = id;
            Name = name;
        }

        public long Id { get; }

        public string Name { get; }

        public bool Equals(User other)
        {
            return Id == other.Id && Name == other.Name;
        }
    }
}
