﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChatApp.Data.Time;
using ChatApp.Data;

namespace ChatApp.Models.Messaging
{
    public class Message
    {
        public Message(MessageTime timeSent, string messageText, User sender, int serverId)
            : this (timeSent, messageText, sender, false, default)
        {
        }

        public Message(MessageTime timeSent, string messageText, User sender, bool edited, MessageTime timeLastEdited)
        {
            Sender = sender;
            MessageText = messageText;
            TimeSent = timeSent;
            Edited = edited;
            TimeLastEdited = timeLastEdited;
            Id = IdGenerator.GenerateId(Sender.Name, timeSent.UtcTime);
        }

        public User Sender;

        public MessageTime TimeSent { get; }

        public MessageTime TimeLastEdited { get; }

        public string MessageText { get; }

        public bool Edited { get; }

        public long Id { get; }
    }
}
