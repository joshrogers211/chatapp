﻿using ChatApp.Data.Time;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatApp.Models.Messaging
{
    public class MessageGroup
    {
        public MessageGroup(Message message)
            : this (message, message.Sender)
        {
        }

        public MessageGroup(Message message, User sender)
        {
            Messages = new List<Message> { message };
            FirstMessageTime = message.TimeSent;
            Sender = sender;
        }

        public User Sender { get; }

        public List<Message> Messages { get; }

        public MessageTime FirstMessageTime { get; }

        public bool AddMessage(Message message)
        {
            if (Sender == message.Sender)
            {
                Messages.Add(message);
                return true;
            }

            return false;
        }
    }
}
