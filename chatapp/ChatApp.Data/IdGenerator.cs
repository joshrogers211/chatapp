﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatApp.Data
{
    public static class IdGenerator
    {
        public static long GenerateId(string strVal, DateTime timeVal)
        {
            return timeVal.Ticks ^ strVal.GetHashCode();
        }
    }
}