﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatApp.Data.Time
{
    public static class TimeHelper
    {
        public static bool ShouldMessagesGroup(MessageTime firstMessageTime, TimeSpan groupingWindow)
        {
            return firstMessageTime.GetLocalTime() + groupingWindow < DateTime.Now;
        }

        public static bool ShouldMessagesGroup(MessageTime firstMessageTime, object groupingWindow)
        {
            return ShouldMessagesGroup(firstMessageTime, (TimeSpan)groupingWindow);
        }
    }
}
