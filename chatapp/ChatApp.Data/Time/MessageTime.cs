﻿using System;
using System.Globalization;

namespace ChatApp.Data.Time
{
    public class MessageTime
    {
        public MessageTime (DateTime localTime, TimeZoneInfo timeZone)
        {
            UtcTime = TimeZoneInfo.ConvertTimeToUtc(localTime, timeZone);
            TimeZone = timeZone;
        }

        public DateTime UtcTime { get; }

        public TimeZoneInfo TimeZone { get; }

        public DateTime GetSenderTime()
        {
            return TimeZoneInfo.ConvertTimeFromUtc(UtcTime, TimeZone);
        }

        public DateTime GetLocalTime()
        {
            return TimeZoneInfo.ConvertTimeFromUtc(UtcTime, TimeZoneInfo.Local);
        }

        public string GetSenderTimeString()
        {
            return GetSenderTime().ToString();
        }

        public string GetLocalTimeString()
        {
            return GetLocalTime().ToString();
        }
    }
}
